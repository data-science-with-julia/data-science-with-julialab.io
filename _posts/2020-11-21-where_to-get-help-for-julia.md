---
title: Where to get help for Julia?
layout: post
---

# What is Julia?

Many in my country don't know what is Julia,rather than me explaining what is Julia, let some of its creators explain it:

{% youtube aMyAHs2c0_w %}

{% youtube qGW0GT1rCvs %}

# The Julia Discourse

Okay, I have been accustomed to Julia for a while, those who are coming new can get help here <https://discourse.julialang.org/>, its the official Julia forum.

# For this blog

If you have doubts in these blogs you can contact me at one of these places:

* The best way is to ask questions here <https://t.me/julia_programmers> or here <https://t.me/ds_ai>
* Or shoot me an email <mindaslab@protonmail.com> - I am really lazy and hence will open mails once in a month

# About Julia community

Most of the Julia community are academics of high degree like Ph.D, so unlike the Ruby and Python community where people are welcoming, these people could be very repulsive, judgmental and could act with prejudice. So if you have bad experience, just brush it away. The three places I said above seems to be okay till now.
