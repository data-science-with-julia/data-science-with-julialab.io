---
title: Starting with Julia (using Jupyter) lab
layout: post
---

{% youtube aaMsJawnJuQ %}

Okay, let's start with Juyter lab. I am using Jupyter notebooks no more because I feel lab is cool. To start it you can either do it by the Julia way by calling IJulia as I mentioned [Installing Jupyter notebook and Jupyter lab]({% post_url 2020-10-31-installing-jupyter-notebook-and-jupyter-lab %}), or I like the Conda way, to do it, in your terminal type this:

```
$ jupyter lab
```

When started in IJulia way, the lab asks me what kernel I must use as shown below:

![](/images/jupyter_lab_selecting_julia_kernel.png)

when started the Conda way, it sows something like this:

![](/images/jupyter_lab_launcher.png)

In the above launcher, I select Julia 1.5.2 in the Notebook section:

![](/images/jupyter_lab_selecting_the_kernel.png)

When you select a kernel, at the left pane you should be seeing a file called `untitled.ipynb`, right click on it and change it's name to `starting_with_julia_lab.ipynb`. Now this lab is like REPL on steroids, it has got thing called cells as shown:

![](/images/jupyter_lab_cell.png)

Now type

```julia
1 + 2
```

in the cell and press `Shift + Enter`, you see the outputs `3` and the input cell receives a number `[1]:`, the output too receives number `[1]` as shown in the image below

![](/images/jupyter_lab_executing_a_command.png)

Now let's try to print the legendary [Hello World](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program) in the next cell

```julia
println("Hello World")
```

It should spit out an output as shown:

    Hello World


Now let's print `1 + 2 = 3`, for that type the following in the cell

```julia
println("1 + 2 = ", 1 + 2)
```

and press `Shift + Enter` or `Ctrl + Enter`

    1 + 2 = 3

You will get an output as shown above

in the coming example, we put a value `3` into a thing called variable, this variable is called `a` and you know how to execute this, go on:

```julia
a = 3
```
Output:

    3


Similarly we put a value `5` in variable `b` and execute it:

```julia
b = 5
```

Output:

    5


Now let's check if 1 equals 2 in the next cell:


```julia
1 == 2
```

Output:

    false

Once you have done this your notebook should look like this.

![](/images/starting_with_julia_lab.png)

A Jupyter lab instance is collection of many notebooks. If you did not understand what I mean by that, its okay, things will clear up.

Instead of `Shift + Enter`, you can press `Alt + Enter`, in that case the cell will be executed, and a new cell won't be created beneath it. To get the code / note book for this blog check out this link <https://gitlab.com/data-science-with-julia/code/-/blob/master/starting_with_julia_lab.ipynb>

Go on and play around with Jupyter lab, mess it up and crash it, start it all over to learn.
