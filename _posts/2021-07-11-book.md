---
title: Book
layout: post
---

Hello folks, there is a chance that I will be launching a book titled Data Science With Julia, may be later than sooner. It will be a electronic only, continuously evolving book. I got the cover's designed and here they are:


|![](/images/book_front_cover.png)|
|:-:|
|Front Cover|

|![](/images/book_back_cover.png)|
|:-:|
|Back Cover|

This book will be absolutely gratis. The book will be released under [GFDL](https://www.gnu.org/licenses/fdl-1.3.html), so you can modify it redistribute it, give it to any one who you want without worrying that some one would sue you.

So! Let's see what the future holds.
