---
title: "Julia REPL - Part 3: Package Management"
layout: post
---

{% youtube 6_pcQB9n9jI %}

Let's see how to add packages in Julia using it's REPL. For that just like you went into help mode by pressing `?` at `julia>` prompt, you must for package management press `]`. To come out of this package `pkg>` thing press `Backspace`.

In case I am going too fast, let's discuss what packages are. Today is the age of internet, and hence a code written by one can be used by millions, all it needs is a internet connection. So almost all modern languages have a way of managing reusable shared code. In Julia, this reusable shared code is called a Package.

Since Julia's ecosystem is open source (I wish people understood the merits of [Free Software](https://fsf.org) and ignored the misleading Open Source, any way), many people look at popular packages in Julia and analyze them, they are well maintained and tested and can used with fair amount of trust. So let's see how to install a package.

# Installing packages

So to make our learning useful, let's install a package that highlights out Julia code typed in the Julia prompt. So press `]` and type `add OhMyREPL` and press `ENTER`, this will spit some output as shown:

```julia
(@v1.5) pkg> add OhMyREPL
 Installing known registries into `~/.julia`
######################################################################## 100.0%
      Added registry `General` to `~/.julia/registries/General`
  Resolving package versions...
  Installed Pipe ───── v1.3.0
  Installed Tokenize ─ v0.5.8
  Installed OhMyREPL ─ v0.5.9
  Installed Crayons ── v4.0.4
  Installed fzf_jll ── v0.21.1+0
  Installed JLFzf ──── v0.1.2
Downloading artifact: fzf
Updating `~/.julia/environments/v1.5/Project.toml`
  [5fb14364] + OhMyREPL v0.5.9
Updating `~/.julia/environments/v1.5/Manifest.toml`
  [a8cc5b0e] + Crayons v4.0.4
  [1019f520] + JLFzf v0.1.2
  [5fb14364] + OhMyREPL v0.5.9
  [b98c9c47] + Pipe v1.3.0
  [0796e94c] + Tokenize v0.5.8
  [214eeab7] + fzf_jll v0.21.1+0
  [2a0f44e3] + Base64
  [ade2ca70] + Dates
  [b77e0a4c] + InteractiveUtils
  [76f85450] + LibGit2
  [8f399da3] + Libdl
  [56ddb016] + Logging
  [d6f4376e] + Markdown
  [44cfe95a] + Pkg
  [de0858da] + Printf
  [3fa0cd96] + REPL
  [9a3f8284] + Random
  [ea8e919c] + SHA
  [9e88b42a] + Serialization
  [6462fe0b] + Sockets
  [cf7118a7] + UUIDs
  [4ec0a83e] + Unicode
```

Hope all went well, now press `Backspace` to come back to Julia prompt

`julia>`

Now press `Ctrl + d` to come out of Julia.

Open a editor, open the file `~/.julia/config/startup.jl`, if it's not present create it, and add this code in it:

```julia
atreplinit() do repl
    try
        @eval using OhMyREPL
    catch e
        @warn "error while importing OhMyREPL" e
    end
end
```

The above code tells to include the package `OhMyREPL` during startup of `julia>` prompt. Save the file. Now in terminal type:

```
$ julia
```

to start Julia REPL, and since this is the first time you are using `OhMyREPL`, julia will compile it and you will see something as shown below:

![](/images/ohMyREPL.png)

So when you try out expressions as shown above, it's color highlighted! Enjoy!!!

# Removing packages

To remove a package, you need to do this

```julia
pkg> rm PackageName
```
But I don't want to remove any thing now.

**Reference**

- <https://docs.julialang.org/en/v1/stdlib/Pkg/>
- <https://juliapackages.com/p/ohmyrepl>

