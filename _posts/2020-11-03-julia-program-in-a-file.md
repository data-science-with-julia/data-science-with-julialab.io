---
title: Julia program in a file
layout: post
---

{% youtube _oDSlQuny4Y %}

It's not that all your Julia programs will be in Jupyter Lab and Jupyter notebooks. Once you have done some thing and its kinda set in stone, you can put in a file and and execute it using the Julia run time. In production environment, its nothing but few Julia files getting executed when needed. In this blog we will see how to put Julia programs in a file and execute it.

I hope you have a text editor with you, I strongly do not recommend using Juptyer lab to create Julia files. Why? Because I don't like it and you will see for yourself if you don't realize it now. I would suggest one to use [Atom](https://atom.io) or [VS Code](https://code.visualstudio.com/), unfortunately both created by evil Microsoft corporation.

Create a file called `hello_world.julia` and put this stuff into it:

```julia
println("Hello World!")
println("Let's see what miracles the new field of Data Science can do.")
```

you can get this source code here <https://gitlab.com/data-science-with-julia/code/-/blob/master/hello_world.julia>

Now in your terminal, go to the directory where the file is and type this:

```
$ julia hello_world.julia
```

and hit `Enter`. If you get this:

```
Hello World!
Let's see what miracles the new field of Data Science can do.
```

as output, then all is well. Yaay, you have created and executed your first Julia program as a file. In data science we explore a lot with notebooks and lab because of their visual nature. When things are okay, we put them in Julia files and ship it off.
