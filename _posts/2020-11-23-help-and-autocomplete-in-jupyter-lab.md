---
title: Help and Autocomplete in Jupyter Lab
layout: post
---

We could have typed out Julia programs in file and have executed it, but we are using Jupyter lab because its more visual and offers lot of goodies. We will talk about two of them in this blog.

![](/images/getiing_help_in_jupyter_2.png)

Take a lot at the image above, I typed `setd` and pressed the `Tab` key and Jupyter gives me a option that I might be looking for `setdiff()` function. So if you want to do autocomplete in Jupyter, use the `Tab` key.

![](/images/atocomplete_in_jupyter.png)

In the example above too I am using the `Tab` key to suggest an auto complete for a variable. I typed partial variable name and pressed `Tab` and it gave me the above recommendations.

The below example is more cool, inside the brackets of `intersect()` function and I press `Shift + Tab`:

![](/images/getting_help_in_jupyter_notebook.png)

and I get a beautiful help documentation as shown. When you learn about functions you will see how to generate these documentation so that when some one uses it and wants to get an explanation for the function you have written they can get help right in Jupyter lab.
