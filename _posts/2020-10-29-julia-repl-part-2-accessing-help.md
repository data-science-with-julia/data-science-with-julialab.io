---
title: "Julia REPL - Part 2: Accessing help"
layout: post
---

{% youtube BC11KlSQm6E %}

If you have been using GNU/Linux or Python (even on on evil platforms like Mac and Windows), you will have a way of accessing help documentation in REPL, so does Julia too. To access help, just type `?` in Julia prompt, you will be presented with something like this:

```julia
help?>
```
You can come out of help by pressing `Backspace`. Okay lets see what `typeof` (if you have read previous blog you may have used `typeof`) does. Just type `typeof` in help prompt and hit `Enter`.

```julia
help?> typeof
search: typeof typejoin TypeError

  typeof(x)

  Get the concrete type of x.

  Examples
  ≡≡≡≡≡≡≡≡≡≡

  julia> a = 1//2;

  julia> typeof(a)
  Rational{Int64}

  julia> M = [1 2; 3.5 4];

  julia> typeof(M)
  Array{Float64,2}
```

Isn't it useful?
