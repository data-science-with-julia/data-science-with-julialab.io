---
title: Boolean Algebra with Julia (oops Should be bitwise operators)
layout: post
---

{% youtube NTxTXxbF5gY %}

**Note:** This blog should have been named bitwise operator, but yes will correct it after some time. Renaming it now will break the link and will leave many with a a 404 page.

Okay, today let's look at [Boolean algebra](https://en.wikipedia.org/wiki/Boolean_algebra) with Julia. The notebook for this blog is here <https://gitlab.com/data-science-with-julia/code/-/blob/master/binary_arithmetic.ipynb>

In Julia, something true is represented by a constant `true` and something false is represented by a constant `false`. These two things are inbuilt in Julia and you can't keep a variable like `true = 1`, Julia will throw an error.

Now let's look at operators that do binary arithmetic. The and operation is done using the and `&` operator as shown:

```julia
true & true
```

Output:


    true




```julia
true & false
```

Output:


    false


We use the pipe `|` symbol to do the OR operation as shown:

```julia
false | true
```

Output:


    true




```julia
false | false
```


Output:

    false


The tilde `~` symbol is used for a NOT as shown:


```julia
~ false
```


Output:

    true


⊻ does the XOR operation, to type it, type `\xor` and press `Tab` key.


```julia
true ⊻ true
```

Output:


    false




```julia
true ⊻ false
```

Output:


    true




```julia
false ⊻ false
```

Output:


    false


The `>>` is used for arithmetic right shift, so 3 divided by 2 is kinda one.

```julia
3 >> 1
```

Output:


    1


The `>>>` is used for logical right shift


```julia
3 >>> 1
```

Output:


    1



In the below example I have no clue why it became -2, may be I will explore it in later blogs

```julia
-3 >> 1
```

Output:


    -2


I am blown away why it produces such a result in the example below, may be I will investigate when I am not lazy:

```julia
-3 >>> 1
```


Output:

    9223372036854775806


The `<<` is used for arithmetic left shift, so shifiting `101` by `1` becomes `1010` which is 6 as shown below:

```julia
3 << 1
```


Output:

    6



Now let's try out some [De Morgan's Law](https://en.wikipedia.org/wiki/De_Morgan's_laws):


```julia
x = true
y = false

~(x | y) == ~x & ~y
```

Output:


    true


and an another one:


```julia
~(x & y) == ~x | ~y
```


Output:

    true

