---
title: "Plots 2: Basic plot Function"
layout: post
---

I think this will be one of my last blog about plots because I'm hasty. I want to explore genetic algorithms based on [Driving a car with genetic algorithm]({% post_url 2020-12-05-driving_a_car_with_genetic_algorithm %}). Any way let's dive in. The Jupyter notebooks for this blog is available here <https://gitlab.com/data-science-with-julia/code/-/blob/master/plots.ipynb>.

# Plotting Sin and Cos

To start with let's define `θ` to be from `-2π` to `2π` with steps of `0.01` as shown below:

```julia
θ = -2π : 0.01 : 2π
```

Output:

    -6.283185307179586:0.01:6.276814692820414

Now lets plot `cos` and `sin` values of `θ`

```julia
plot(
    θ, [sin.(θ), cos.(θ)],
    title = "sin and cos plots",
    xlabel = "Radians",
    ylabel = "Amplitude",
    label = ["sin(θ)" "cos(θ)"],
    ylims = (-1.5, 1.5),
    xlims = (-7, 7)
)
```

Output:

![svg](/images/plots/plots_2/output_4_0.svg)

So, what just happened? We have a function called `plot()`:

```julia
plot()
```

To that we pass the first argument to be `θ`:

```julia
plot(θ)
```

the second argument to be an array of `sin` and `cos` of `θ`:

```julia
plot(θ, [sin.(θ), cos.(θ)])
```

apart from that we pass the following attributes too:

- `title` : Self explanatory
- `xlabel` : The text that must appear for the x-axis
- `ylabel` : The text that must appear for the y-axis
- `label` : The legend of the curves shown in top right
- `xlims` and `ylims` : The minimum and maximum of coordinates that needs to be displayed for the plot, these accepts `Tuple` as argument


# Building a Plot

Its not that you have to create a plot all in a go, you can build it part by part, you can see blow that we plot `sin`of `θ` below, we have set `linewidth = 3` so it appears nice and thick, we have set the color to be purple, and its label to be sin θ:

```julia
p = plot(θ, sin.(θ), color = "purple", linewidth = 3, label = "sin θ")
```

so you get a plot as shown:


![svg](/images/plots/plots_2/output_5_0.svg)

we have assigned the output of the above plot to a variable named `p`, so in the future this could be modified. Now lets add `cos` plot to `p`, for that look at the code below:


```julia
plot!(p, θ, cos.(θ), color = "red", linewidth = 3, label = "cos θ")
```

Output:


![svg](/images/plots/plots_2/output_6_0.svg)


so it is the same with just one difference, we are mutating `p` by passing it to the `plot!()` function. Notice the exclamation mark `!` in the `plot!()` and it accepts a plot as its first argument which it changes. Now in the code below we further decorate `p` with title, labels and limits:

```julia
plot!(
    p,
    title = "sin and cos plots",
    xlabel = "Radians",
    ylabel = "Amplitude",
    ylims = (-1.5, 1.5),
    xlims = (-7, 7)
)
```
Output:

![svg](/images/plots/plots_2/output_7_0.svg)

