---
title: "Plots 3: Scatter and Histogram"
layout: post
---

I thought of ending blogs about plots last time, but I did think about Scatter and Histogram plots. I feel they are important and I felt I need them to better explain some of the blogs I kinda a plan to write in the future. So here are scatter and histogram plots.

So let's create a simple scatter, x-values as first argument, y-values as second, I have given color as red but its completely optional:

```julia
scatter_plot = scatter([1, 2 , 3,  4], [1, 2, 3, 4], color = "red")
```

Output:

![svg](/images/plots/plots_3/output_8_0.svg)


We have captured last output in a variable called `scatter_plot`, this is much better variable name I tell you. In my [previous blog]({% post_url 2020-12-12-plots-2-basic-plot-function %}) I had given a names like `p` which is actually blasphemy in programming world, you might be smart today and understand what `p` is, but possibly 6 months later it will haunt you, or a programmer touching your code sometime later might curse you thus causing your loved ones to vomit blood and die. Anyway, we now add another scatter to `scatter_plot` using the code below:


```julia
scatter!(scatter_plot, [1, 7, 8, 2, 3], [2, 6, 3, 1, 4], color = "blue")
```

Output:

![svg](/images/plots/plots_3/output_9_0.svg)

As you can see above, this time we have not used `plot!()` to modify `scatter_plot` but we have used this function `scatter!()` which I personally feel is much better to read. And we have put these new dots in color blue. Let's modify `scatter_plot` again this time using `plot!()` function as shown below:


```julia
plot!(
  scatter_plot,
  [1, 2, 3, 4 ],
  [-1, -2 , -3, -4],
  color = "orange",
  seriestype = :scatter
)
```

Output:


![svg](/images/plots/plots_3/output_10_0.svg)


As you see above in the above `plot!()` we have used named argument `seriestype` and have set it to `:scatter` for it to be a scatter plot. I am unsure why we have a colon `:` before scatter, should check Julia docs about it. Okay, looks like this is a special kind of thing known as `Symbol`, another data type in Julia, possibly it occupies less space compared to `"scatter"` which is a string when called multiple times. I just checked this code:

```julia
julia> :a
:a

julia> typeof(:a)
Symbol
```

just to check its type.

Okay, the label box int above scatter plots is overlapping a data point, lets increase the x right limit to 12 so that it would look better:

```julia
plot!(scatter_plot, xlims = (0, 12))
```

Output:

![svg](/images/plots/plots_3/output_11_0.svg)

Better now!

For some reason I like histograms, so I have plotter a histogram below:


```julia
histogram(rand(1:1000, 500), bins = 20)
```

Output:

![svg](/images/plots/plots_3/output_12_0.svg)

I think I will be using it while writing about the [Iris data set](https://en.wikipedia.org/wiki/Iris_flower_data_set). You can get the notebook file for this blog here <https://gitlab.com/data-science-with-julia/code/-/blob/master/plots.ipynb>.

# Learn more about Plots

This is the last blog about Plots I think unless i change my mind for some reason, if you want to learn more check the official Plots website <https://docs.juliaplots.org/latest/>, and this Julia Plots by Prude University <https://www.math.purdue.edu/~allen450/Plotting-Tutorial.html> is good too.
