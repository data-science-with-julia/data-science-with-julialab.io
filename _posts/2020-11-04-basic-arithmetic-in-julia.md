---
title: Basic Arithmetic
layout: post
---

{% youtube 4g0eImxZ_K4 %}

Let us see about basic arithmetic in Julia. The notebook for this blog is available here <https://gitlab.com/data-science-with-julia/code/-/blob/master/artthmetic.ipynb>.

First start a notebook, so first let's do addition. Type this in a cell:

```julia
1 + 2
```
and execute it. As you can see `+` sign adds two numbers and outputs the result as shown:

    3


Try an other example:

```julia
41 + 1
```

Output:

    42


Now let's try multiplication:

```julia
6 * 7
```

The `*` sign multiplies numbers at its left and right side and outputs the result as shown:

    42


Now let's try subtraction which is accomplished by `-` sign. In the example below we are taking away 8 from 50:

```julia
50 - 8
```

Output:

    42


Now division is done by `/`, so this is what you get when you divide 375 by 21:

```julia
375 / 21
```

Output:

    17.857142857142858

Now $$x^y$$ in Julia can be written as `x ^ y`, so the below example finds out 28 raised to the power of 12:

```julia
28 ^ 12
```

Output:

    232218265089212416


For curiosity reason we will see what will happen when we add -2 and 45:


```julia
-2 + 45
```

Output:

    43


There is a operator called mod `%` which gives us the reminder. Lets see what's the reminder when 21 is divided by 4:

```julia
21 % 4
```

Output:

    1

We have seen how to get reminder, now let's see how to get quotient of a division operation. For that we use `÷` operator which does integer division. So look at the example below:

```julia
21 ÷ 4
```

Output

    5

To type in `÷` in the notebook, type `\div` in a cell and press `Tab`.

It's not that one can enter only 1 statement in Jupyter lab's cell and press `Shift + Enter` to execute it, you can enter multiple statements by using `Enter` key at the end of statement, then you can press `Shift + Enter` to execute it.

In the example below, we convert 90 degree Celsius to Fahrenheit.


```julia
celcius = 90
fahrenheit = (9 / 5)celcius + 32
```

Output:

    194.0

In the above example, first we assign value `90` to variable named `celcius` using this statement `celcius = 90`. Next we compute Fahrenheit as follows `fahrenheit = (9 / 5)celcius + 32`. Look how we can give `(9 / 5)celcius`, Julia can find out you are trying to multiply $$9 \over 5$$ with `celcius`, it's very much like writing an equation. Then `32` is added to it here `(9 / 5)celcius + 32`.

For the sake of experimentation try out this statement `fahrenheit = (9 / 5) * (celcius + 32)`. Did you get the right answer? What went wrong?


Now try out the example below:


```julia
celcius = 90
fahrenheit = (9 / 5) celcius + 32
```

Output:

    syntax: extra token "celcius" after end of expression



    Stacktrace:

     [1] top-level scope at In[14]:2

     [2] include_string(::Function, ::Module, ::String, ::String) at ./loading.jl:1091

     [3] execute_code(::String, ::String) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:27

     [4] execute_request(::ZMQ.Socket, ::IJulia.Msg) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:86

     [5] #invokelatest#1 at ./essentials.jl:710 [inlined]

     [6] invokelatest at ./essentials.jl:709 [inlined]

     [7] eventloop(::ZMQ.Socket) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/eventloop.jl:8

     [8] (::IJulia.var"#15#18")() at ./task.jl:356


Note how quickly Julia could get confused by just leaving a space between `(9/5)` and `celcius`, wheras you human can see its a obvious thing.

Now try this example out:

```julia
celcius = 90
fahrenheit = (9 / 5) * celcius + 32
```

Output:

    194.0

There is a difference between how Scientists write equations and programmers program it. For example for a scientist writing `F = (9/5)C + 32` is a good enough Julia program, but as a programmer one would absolutely hate it. A programmer would like the program to be self documenting. Hence he would like `fahrenheit = (9 / 5) * celcius + 32`. He would like to mention `celcius` is multiplied by `(9 / 5)` explicitly using the `*` operator.

For a scientist `(9/5)` is good enough, but for a programmer its blasphemy. Programmers like to the program to be more readable. I think I need to write a blog about the way of code: Scientists Vs Programmers. `(9 / 5)` is more readable than `(9/5)` programmer would say.

Now consider the example below. When ever you wrap something in brackets, it get's executed first, so in the below operation `90` is added with `32` in `(celcius + 32)` giving `122`, then this `122` is multiplied with $$9 \over 5$$ in `(9 / 5) * (celcius + 32)`:


```julia
celcius = 90
farenheit = (9 / 5) * (celcius + 32)
```

Output:

    219.6


You can use only round brackets `(` and this `)` in math iperations in Julia, curly and other brackets are strict no:

```julia
celcius = 90
fahrenheit = {9 / 5} * celcius + 32
```

Output:

    syntax: { } vector syntax is discontinued around In[17]:2



    Stacktrace:

     [1] top-level scope at In[17]:2

     [2] include_string(::Function, ::Module, ::String, ::String) at ./loading.jl:1091

     [3] execute_code(::String, ::String) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:27

     [4] execute_request(::ZMQ.Socket, ::IJulia.Msg) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:86

     [5] #invokelatest#1 at ./essentials.jl:710 [inlined]

     [6] invokelatest at ./essentials.jl:709 [inlined]

     [7] eventloop(::ZMQ.Socket) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/eventloop.jl:8

     [8] (::IJulia.var"#15#18")() at ./task.jl:356


Here I am trying to use square brackets:

```julia
celcius = 90
fahrenheit = [9/5] * celcius + 32
```

Output:

    MethodError: no method matching +(::Array{Float64,1}, ::Int64)
    For element-wise addition, use broadcasting with dot syntax: array .+ scalar
    Closest candidates are:
      +(::Any, ::Any, !Matched::Any, !Matched::Any...) at operators.jl:538
      +(!Matched::Missing, ::Number) at missing.jl:115
      +(!Matched::Base.CoreLogging.LogLevel, ::Integer) at logging.jl:116
      ...



    Stacktrace:

     [1] top-level scope at In[18]:2

     [2] include_string(::Function, ::Module, ::String, ::String) at ./loading.jl:1091

     [3] execute_code(::String, ::String) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:27

     [4] execute_request(::ZMQ.Socket, ::IJulia.Msg) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:86

     [5] #invokelatest#1 at ./essentials.jl:710 [inlined]

     [6] invokelatest at ./essentials.jl:709 [inlined]

     [7] eventloop(::ZMQ.Socket) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/eventloop.jl:8

     [8] (::IJulia.var"#15#18")() at ./task.jl:356

One good thing about Julia is you can assign values to variables and construct algebric equations as follows:

```julia
x = 4
y = 7

3x + 4y + 27
```

Output:

    67

Try breaking the above example and make it throw errors so that you would learn.


Let's assign `12` to `z` as shown:

```julia
z = 12
```

Output:

    12

Below example is the compact way of writing `z = z + 4`, that is you are adding `4` with `z`, making it `16` and assigning it to `z` again.

**Example 1:**

```julia
z += 4
z
```

Output:

    16

Imagine what would a mathematician think if you present an equation $$z = z+4$$ :D Programmer and the way maths and science people think are very different, but data science is marriage of huge amounts of data with computer processing which run algorithms based on scientific observation and mathematics.

Since I am lazy, explain to your self what's happening in the examples below:

**Example 2:**


```julia
z -= 3
z
```

Output:

    13



**Example 3:**

```julia
z *= 17
z
```

Output:

    221


**Example 4:**

```julia
z %= 5
```

Output:

    1


**Example 5:**

```julia
z = 12
z ^= 2
z
```

Output:

    144


