---
title: Dictionaries
layout: post
---

{% youtube 1U2XSGMSJWg %}

What comes to your mind when you think of a dictionary? Today in software or online dictionary you type a word and get its meaning in seconds. When I was small we had paper books, in each page there were printed lot of words and its meaning, even then we could find meaning of a word within one or two minutes. That's because dictionary was a ordered set of words with which we could pin point the word were looking with ease. In programming languages dictionaries are designed in similar fashion, it could book looked up with ease.

In a language dictionary, you can think of a looked up word as a key or index, the meaning that is entered against the word as a value. Take a look at the code below, type it and execute it:

```julia
spell = Dict(
    1 => "one",
    2 => "two",
    3 => "three"
)
```

Output:

    Dict{Int64,String} with 3 entries:
      2 => "two"
      3 => "three"
      1 => "one"


In the above example we create a dictionary using the `Dict()` function, the keys are `1`, `2` and `3`, we have given their English names as their values. Now if you want to get the spelling of `3`, you can get it as shown:

```julia
number = 3
spell[number]
```

Output:

    "three"

One who knows arrays can argue this can be achieved with the following code:

```julia
spell = ["one", "two", "tree"]
spell[3]
```

that's correct, but take the example as shown below:

```julia
prices = Dict(
    "mango" => 50,
    "bananna" => 10,
    "samosa" => 15,
    "briyani" => 80
)
```

Output:

    Dict{String,Int64} with 4 entries:
      "mango"   => 50
      "samosa"  => 15
      "briyani" => 80
      "bananna" => 10

above we have created a dictionary with prices of items, now say is it better for one to access price of a mango by giving `prices[1]`, or `prices["mango"]`?

Let's say we went to shop and bought two Briyani parcels and four Samosas, I'm starting to feel hungry now 😋. We can represent our purchase as shown:


```julia
purchase = Dict(
    "briyani" => 2,
    "samosa" => 4
)
```

Output:


    Dict{String,Int64} with 2 entries:
      "samosa"  => 4
      "briyani" => 2


Just to know more about dictionaries, this is how you would list keys of a dictionary:

```julia
keys(purchase)
```

Output:


    Base.KeySet for a Dict{String,Int64} with 2 entries. Keys:
      "samosa"
      "briyani"

This is how you will list values contained in the dictionary:

```julia
values(purchase)
```

Output:


    Base.ValueIterator for a Dict{String,Int64} with 2 entries. Values:
      4
      2

The `length()` function works in a dictionary and returns the count of key value pairs in it.


```julia
length(purchase)
```

Output:


    2

`typeof()` seems to be a universal function, and it works on dictionaries as well:


```julia
typeof(purchase)
```

Output:


    Dict{String,Int64}


Now let's calculate our bill, type the program below and see what happens:

```julia
total = 0

for (item, quantity) in purchase
    total += prices[item] * quantity
end

total
```

Output:


    220

So let me explain how the program works. First we have assigned a variable `total` to `0` here `total = 0`.

Next look at this statement:

```julia
for (item, quantity) in purchase
```

look how we unravel the key value pair as `(item, quantity)`. Since we are using a `for` loop, for every purchase, the key gets stored in variable called `item` and value get's stored in variabe called `quantity`. Now for the first iteration it would would be like this:

`total += prices[item] * quantity`

Which would translate into `total = 0 + (prices["briyani"] * 2)`, which will be reduced to `total = 80 * 2`, hence total will become `160`, and the loop goes on and on till all the `purchase` elements are fetched and the `total` calculated, and we get a grand total of `220`

Since we know list comprehension, we can write the above code as shown below:

```julia
sum([prices[item] * quantity for (item, quantity) in purchase])
```

Output:


    220

Neat isn't it?
