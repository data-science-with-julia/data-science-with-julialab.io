---
title: Installing Julia
layout: post
---

{% youtube XmVrqbkhYyI %}

In this blog we will see how to install Julia. Visit <https://julialang.org>, you must see a banner like this which offers download. Click on the Download button

![](/images/downloading_julia_1.5.2_step_1.png)

You will be a taken to this page <https://julialang.org/downloads/>, if you scroll down you will see something like this:

![](/images/downloading_julia_1.5.2_step_2.png)

I clicked on the 64-bit thing for Generic Linux x86. Once you do that a file named `julia-1.5.2-linux-x86_64.tar.gz` will be downloaded. Extract it to your home directory and open `~/.bashrc` and add it this at the last:

```
# Add Julia to PATH
export PATH="$PATH:$HOME/julia-1.5.2/bin"
```

this tells your computer to look into `~/julia-1.5.2/bin` when you call `julia`. Just for once type this in terminal:

```
$ source ~/.bashrc
```

To know more about `.basrc` you can visit here <https://www.journaldev.com/41479/bashrc-file-in-linux>.

In your terminal when you type `julia`, you should be getting this:

```
$ julia
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.5.2 (2020-09-23)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

julia>

```

Press `Ctrl+D` to exit.

If all had gone well, you had installed Julia, ran its REPL, and came out of it successfully. In case you are wondering what REPL is, wait for the next blog.
