---
title: Conditions and Branching
layout: post
---

{% youtube ROvLW1-sM-w %}

In the last example we have seen how to use comparisons and to compare things. Now let's see how to use them. Let's see how to compare two numbers and print useful output. The notebook for this blog is here <https://gitlab.com/data-science-with-julia/code/-/blob/master/if.ipynb>.

Take a look at the example below, type it in  your Jupyter lab and execute it:

```julia
a = 5; b = 3

if a > b
    println("a = ", a, " is bigger")
end

if b > a
    println("b = ", b, " is bigger")
end

if a == b
    println("both numbers are equal")
end
```

Output:

    a = 5 is bigger

It says `a = 5 is bigger`, now change the values of `a` and `b` in  `a = 5; b = 3` and see what happens. Make one greater than other, make them both equal and see. Now let's see how it works. The main work horse of the above program is the `if` condition. It's syntax is as follows:

```
if <some condition>
  # Do something here if <some condition> is true
end
```

it consists of a `if` key word, followed by some condition. So in:

```julia
if a > b
    println("a = ", a, " is bigger")
end
```

`a > b` is the condition. If this condition is true then the code block between `if` and `end` will get executed. So in `if a > b`, `a` is 5 and `b` is 3 and hence its true, so the statement `println("a = ", a, " is bigger")` in:

```julia
if a > b
    println("a = ", a, " is bigger")
end
```

gets executed, you get the output `a = 5 is bigger`. Now try explaining the other parts of the program to yourself.

Now in the above program the conditions are checked three times, that is first in `if a > b` then in `if b > a` and then in `if a == b`. What if `a > b` is true, then its just waste of computing resource to check for other conditions. Is there a programming construct that when one condition passes, all others are ignored? Fortunately yes. Check the program below:

```julia
a = 3; b = 5

if a > b
    println("a = ", a, " is bigger")

    elseif b > a
        println("b = ", b, " is bigger")

    elseif a == b
        println("both numbers are equal")
end
```

Output:

    b = 5 is bigger

In the above program `b` is greater than `a`, so when it hits `if a > b` it fails. Then Julia checks if there is a `elseif` keyword. In this case there is. First we have `elseif b > a` and `b > a` is true. So the statement under the `elseif` gets executed and hence `println("b = ", b, " is bigger")` gets executed and it prints `b = 5 is bigger`.

In the above program, we have another condition check `elseif a == b`, in reality either `a` or `b` should be greater or they must be equal, so in fact this will be hit when both conditions `a > b` and `b > a` fail, so we could avoid condition check here and assume both numbers are equal and we can write the program as shown:

```julia
a = 3; b = 3

if a > b
    println("a = ", a, " is bigger")

    elseif b > a
        println("b = ", b, " is bigger")

    else
        println("both numbers are equal")
end
```

Output:

    both numbers are equal

Take a look at the above program. Look at the:

```julia
else
    println("both numbers are equal")
```

This `else` is hit only when all other conditions have failed. So when neither `a` or `b` is greater, they both must be equal. Hence there is no need of a condition check here. `else` always has no condition in it and is executed when all other conditions in `if` and `elseif` fails.

So since all conditions fail in the above program `println("both numbers are equal")` under the `else` gets execute and we get `both numbers are equal` as output.

