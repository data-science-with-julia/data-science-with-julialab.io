---
layout: post
title:  "What you need to have to start Data Science?"
---

{% youtube 2m93rm-okHU %}

You are reading this because you already know A, B, C, D.... Data Science knowledge wont pop out just like that. Even if you start to learn, you will see that your learning ambition fades out quickly. There is a good chance that this blog too will fade out due to my lack of interest. But, there are certain things that can keep you going. This blog describes what they are.

# Passion

Let's say I get Rs 5 crores in my bank account, may be that's a money that only industrialists that exploit mass labor, celebrities, politicians, crooks and corrupt officials in India will get it. If I get it, will I continue this blog? Perhaps yes. May be I have a passion for Data Science, just like I had passion for web development. Passion is very important.

If you are reading this blog, thinking just about your salary, you will fail. I do think about money, in fact I am keeping these text to kind of secure my future, but I try to build my future security on computer science and passion for stuff I have.

# Time

You can't become a data scientist by taking a online course for 3 months, or perhaps even by getting a college degree. You need to practice a lot. Data science is logic and mathematics and your brain is not logical. So you need to have lot of time yo pursue the discipline.

You may not not understand some concepts so you may need to step back, take time understand it, all of a sudden things will click and you will race ahead. All of this takes time and patience. Patience is fast diminishing from human society, so it might take lot of mental effort to become a Data Scientist.

# A good computer

You do need a good computer. Definitely don't use a  Windows or a Mac which are built for fools who would like to think themselves as geek, they are not meant for serious computing people. If you are really passionate try building a Ryzen desktop, install Ubuntu GNU/Linux <https://ubuntu.com> in it. If you like to solve lot of problems, try to install Arch GNU/Linux <https://www.archlinux.org> in it.

Or if you need a ready made solution, I would recommend System 76 <https://system76.com>.

# Knowledge of GNU/Linux

Knowledge of GNU/Linux is absolutely essential in Data Science world. Believe it or not, many of Data Science work can be done with GNU/Linux tools. Deploying data science solutions will definitely happen on GNU/Linux, plus your learning will be smooth if you are practicing on GNU/Linux.

To learn Linux I would recommend Linux Journey <https://linuxjourney.com>

# Knowledge of Mathematics

Data Science is mathematics. You take lot of data, use Computer Science to do math  operations on them and get a result. Data Science is marriage of lot of Data and Computer Science. In these blogs I will be teaching math too as these are supposed to be comprehensive and aid my understanding if I forgot or got out of touch of something.

For now to learn Algebra, Matrices, Probability and Statistics, Calculus from Khan Academy <https://khanacademy.org>.

# Programming knowledge

It is better to have some kind of programming knowledge, this blog will teach you if you are not familiar with programming, but there is a problem; I am a accomplished programmer and while teaching programming I might not think from the shoes of person who is learning afresh, in case I sound too complicated, email me. Do remember its fro my reference, so I might miss out on something, in that case email me, I might pick it up.

To learn about programming all by yourself I would recommend Think Julia <https://benlauwens.github.io/ThinkJulia.jl/latest/book.html> .If would refer this book too to aid these blogs.
