---
title: Should you become a data scientist?
layout: post
---

{% youtube sJciRIZQTg4 %}

A question was asked in my [Data Science telegram group](https://t.me/ds_ai), what course should I take to become a Data Scientist. I can ask back a question **Does taking a course make you a data scientist?**. I'm afraid it's not.

Go into data science if you have an idea what it is, just because its a hype word don't go into it, you could loose interest in that subject if you really don't love it. Have a vision of what you can do when you become a data scientist, that vision should not be selling the hype word _Data Science_ and looking for a fool who might pay you.

I am into data science because I was fascinated by artificial intelligence since my  college days. I feel it will become the best friend of man beating fellow humans and biological pets. Imagine an accident victim, he or she might be young but may be needing care in hospital, a human nurse may have her own likes and dislikes, may be she is in her job for money than because she loves to take care of humans like mother Teresa does, her care would be less than ideal for the patient, a robotic care taker that has lerned watching millions of care taking videos can replace her. I am going to get old and die any way, I would like to have a robotic system that takes good care of me than care taker who is in her job for money.

You can see many places where humans many not like to do job. Still in news I see manual laborers found dead in sewage pits and sewage manholes, why not replace them with robots that has power of human thought? People who do not like to cook would like to buy a robotic cook that learns from internet and cooks. There are plenty of applications that might be possible. You must have a dream and go for it incrementally. If you have such a dream, then Data Science is good for you.

# Have interest in math

There are books out there that leave out math just because people don't like it and if included the book won't sell and there are many courses that try to give you an illusion that you do not need mathematics for data science. I strongly feel that is misleading. Data science is dealing with lot of data and applying computer science to it. We will employ lot of statistical methods, and math is important part of it. You must be good and love math to become a successful data scientist.

# Data Science will be boring

Much of the work in Data Science is about getting a data that is very unsuitable for analysis and making it suitable. So most of your work will be to identify series of steps that will make unsuitable data suitable. This is not romantic and cutting edge work. Once you have got the data in right form then its matter of trying various algorithms on it. But 90% of the work is just data cleaning.

Even if you have identified steps to clean the data and strung the cleaning steps together pragmatically which is technically called _pipeline_, your pipeline may break because there was some really unseen needle in hay stack, so you must need to correct it going back to square one. So al these things will take the hell out of you.

# Dealing with humans

You are unfortunately going to work for humans. In technological industry usually people who do not understand tech rise to the top, and people who know it are the work horses. So you ight have to deal with people who are not your caliber and who won't understand you.

I formed a Data Science company with a very nice guy, but found that the person could not understand what I said, though he was a techie, he couldn't think in data science domain. For him _Scikit Learn_ was wrong, _facebook prophet_ was wrong, and all he did was to shout, his math too was wrong. If I said accuracy $$\pm10\%$$, for him it was 80 to 120 rather than 90 to 110. I have to abandon a company which I once called it my own.  Usually good work environment is made up of good boss and good subordinate. It's very rare to get both right.

# Lot of practical experience

Data science needs lot of practical experience, it's like math and programming, the more you practice, the more muscle memory you get, the more intuitive you become, and using that as a base you can scale higher. Dedicate 2 years of your life for studying data science and do an internship or free projects to evaluate your skill. Once you are confident, look for a job or start your consulting.

# Conclusion

The above things are just a brief stuff, I could go on and on. So decide for yourself if you have the things to face these. If yes Data Science might be for you.

{% youtube NZngYDDDfW4 %}
