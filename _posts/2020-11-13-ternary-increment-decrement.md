---
title: Ternary increment decrement
layout: post
---

There was a thread in reddit where a reader of my blog post this one:

```julia
increment = true
a = 5

a += increment ? 1 : -1

println("a = ", a)
```

    a = 6

He was responding to my post [Ternary Operator]({% post_url 2020-11-08-ternary_operator %}) where I would have ranted how increment and decrement with ternary operator though work's, it does not look elegant. I would have proposed doing it with `if else` instead. I must admit that redditor has a kinda elegant solution, but I would still say it's a somewhat less elegant. But yes, this is a blog, so I am allowed to say my personal opinion like Python will die because [Guidio has joined Microsoft](https://techcrunch.com/2020/11/12/python-creator-guido-van-rossum-joins-microsoft/), and Microsoft is known to kill any better software than it could produce for business reasons, which is a fact.

You can also write the program like this:

```julia
∆ = false
a = 5

a += ∆ ? 1 : -1

println("a = ", a)
```

Output:

    a = 4

Where `∆` can  be typed by typing `\increment` and pressing the `Tab` key. Find the Jupyter notebook for this here <https://gitlab.com/data-science-with-julia/code/-/blob/master/increment_decrement_with_ternary_operator.ipynb>.
