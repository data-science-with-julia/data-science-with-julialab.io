---
title: Iris Dataset Analysis - Plotting it.
layout: post
---

|![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Iris_germanica_%28Purple_bearded_Iris%29%2C_Wakehurst_Place%2C_UK_-_Diliff.jpg/564px-Iris_germanica_%28Purple_bearded_Iris%29%2C_Wakehurst_Place%2C_UK_-_Diliff.jpg)|
|:-:|
|The Iris flower|

I happened to go through Iris Data set with Julia and I thought why not blog here? There is a package called RDatasets in Jilia which provides nicely cleaned datasets for machine learning people to start  to learn. You can install it and start using it with Jupyter as shown:

```julia
using Pkg
Pkg.add("RDatasets")
using RDatasets
```

    [32m[1m   Updating[22m[39m registry at `~/.julia/registries/General`
    ######################################################################### 100.0%
    [32m[1m  Resolving[22m[39m package versions...
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Project.toml`
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Manifest.toml`

Once done, lets look at the Data sets:

```julia
RDatasets.datasets()
```


<table class="data-frame"><thead><tr><th></th><th>Package</th><th>Dataset</th><th>Title</th><th>Rows</th><th>Columns</th></tr><tr><th></th><th>String</th><th>String</th><th>String</th><th>Int64</th><th>Int64</th></tr></thead><tbody><p>763 rows × 5 columns</p><tr><th>1</th><td>COUNT</td><td>affairs</td><td>affairs</td><td>601</td><td>18</td></tr><tr><th>2</th><td>COUNT</td><td>azdrg112</td><td>azdrg112</td><td>1798</td><td>4</td></tr><tr><th>3</th><td>COUNT</td><td>azpro</td><td>azpro</td><td>3589</td><td>6</td></tr><tr><th>4</th><td>COUNT</td><td>badhealth</td><td>badhealth</td><td>1127</td><td>3</td></tr><tr><th>5</th><td>COUNT</td><td>fasttrakg</td><td>fasttrakg</td><td>15</td><td>9</td></tr><tr><th>6</th><td>COUNT</td><td>lbw</td><td>lbw</td><td>189</td><td>10</td></tr><tr><th>7</th><td>COUNT</td><td>lbwgrp</td><td>lbwgrp</td><td>6</td><td>7</td></tr><tr><th>8</th><td>COUNT</td><td>loomis</td><td>loomis</td><td>410</td><td>11</td></tr><tr><th>9</th><td>COUNT</td><td>mdvis</td><td>mdvis</td><td>2227</td><td>13</td></tr><tr><th>10</th><td>COUNT</td><td>medpar</td><td>medpar</td><td>1495</td><td>10</td></tr><tr><th>11</th><td>COUNT</td><td>rwm</td><td>rwm</td><td>27326</td><td>4</td></tr><tr><th>12</th><td>COUNT</td><td>rwm5yr</td><td>rwm5yr</td><td>19609</td><td>17</td></tr><tr><th>13</th><td>COUNT</td><td>ships</td><td>ships</td><td>40</td><td>7</td></tr><tr><th>14</th><td>COUNT</td><td>titanic</td><td>titanic</td><td>1316</td><td>4</td></tr><tr><th>15</th><td>COUNT</td><td>titanicgrp</td><td>titanicgrp</td><td>12</td><td>5</td></tr><tr><th>16</th><td>Ecdat</td><td>Accident</td><td>Ship Accidents</td><td>40</td><td>5</td></tr><tr><th>17</th><td>Ecdat</td><td>Airline</td><td>Cost for U.S. Airlines</td><td>90</td><td>6</td></tr><tr><th>18</th><td>Ecdat</td><td>Airq</td><td>Air Quality for Californian Metropolitan Areas</td><td>30</td><td>6</td></tr><tr><th>19</th><td>Ecdat</td><td>Benefits</td><td>Unemployement of Blue Collar Workers</td><td>4877</td><td>18</td></tr><tr><th>20</th><td>Ecdat</td><td>Bids</td><td>Bids Received By U.S. Firms</td><td>126</td><td>12</td></tr><tr><th>21</th><td>Ecdat</td><td>BudgetFood</td><td>Budget Share of Food for Spanish Households</td><td>23972</td><td>6</td></tr><tr><th>22</th><td>Ecdat</td><td>BudgetItaly</td><td>Budget Shares for Italian Households</td><td>1729</td><td>11</td></tr><tr><th>23</th><td>Ecdat</td><td>BudgetUK</td><td>Budget Shares of British Households</td><td>1519</td><td>10</td></tr><tr><th>24</th><td>Ecdat</td><td>Bwages</td><td>Wages in Belgium</td><td>1472</td><td>4</td></tr><tr><th>25</th><td>Ecdat</td><td>CPSch3</td><td>Earnings from the Current Population Survey</td><td>11130</td><td>3</td></tr><tr><th>26</th><td>Ecdat</td><td>Capm</td><td>Stock Market Data</td><td>516</td><td>5</td></tr><tr><th>27</th><td>Ecdat</td><td>Car</td><td>Stated Preferences for Car Choice</td><td>4654</td><td>70</td></tr><tr><th>28</th><td>Ecdat</td><td>Caschool</td><td>The California Test Score Data Set</td><td>420</td><td>17</td></tr><tr><th>29</th><td>Ecdat</td><td>Catsup</td><td>Choice of Brand for Catsup</td><td>2798</td><td>14</td></tr><tr><th>30</th><td>Ecdat</td><td>Cigar</td><td>Cigarette Consumption</td><td>1380</td><td>9</td></tr><tr><th>&vellip;</th><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td></tr></tbody></table>

It's a lot of data, so Jupyter Truncates the output. Now lets load it into a variable called `iris` using the following command:


```julia
iris = dataset("datasets", "iris")
```

As you can see it displays the first 30 rows:

<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>150 rows × 5 columns</p><tr><th>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr><tr><th>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>6</th><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr><tr><th>7</th><td>4.6</td><td>3.4</td><td>1.4</td><td>0.3</td><td>setosa</td></tr><tr><th>8</th><td>5.0</td><td>3.4</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>9</th><td>4.4</td><td>2.9</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>10</th><td>4.9</td><td>3.1</td><td>1.5</td><td>0.1</td><td>setosa</td></tr><tr><th>11</th><td>5.4</td><td>3.7</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>12</th><td>4.8</td><td>3.4</td><td>1.6</td><td>0.2</td><td>setosa</td></tr><tr><th>13</th><td>4.8</td><td>3.0</td><td>1.4</td><td>0.1</td><td>setosa</td></tr><tr><th>14</th><td>4.3</td><td>3.0</td><td>1.1</td><td>0.1</td><td>setosa</td></tr><tr><th>15</th><td>5.8</td><td>4.0</td><td>1.2</td><td>0.2</td><td>setosa</td></tr><tr><th>16</th><td>5.7</td><td>4.4</td><td>1.5</td><td>0.4</td><td>setosa</td></tr><tr><th>17</th><td>5.4</td><td>3.9</td><td>1.3</td><td>0.4</td><td>setosa</td></tr><tr><th>18</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.3</td><td>setosa</td></tr><tr><th>19</th><td>5.7</td><td>3.8</td><td>1.7</td><td>0.3</td><td>setosa</td></tr><tr><th>20</th><td>5.1</td><td>3.8</td><td>1.5</td><td>0.3</td><td>setosa</td></tr><tr><th>21</th><td>5.4</td><td>3.4</td><td>1.7</td><td>0.2</td><td>setosa</td></tr><tr><th>22</th><td>5.1</td><td>3.7</td><td>1.5</td><td>0.4</td><td>setosa</td></tr><tr><th>23</th><td>4.6</td><td>3.6</td><td>1.0</td><td>0.2</td><td>setosa</td></tr><tr><th>24</th><td>5.1</td><td>3.3</td><td>1.7</td><td>0.5</td><td>setosa</td></tr><tr><th>25</th><td>4.8</td><td>3.4</td><td>1.9</td><td>0.2</td><td>setosa</td></tr><tr><th>26</th><td>5.0</td><td>3.0</td><td>1.6</td><td>0.2</td><td>setosa</td></tr><tr><th>27</th><td>5.0</td><td>3.4</td><td>1.6</td><td>0.4</td><td>setosa</td></tr><tr><th>28</th><td>5.2</td><td>3.5</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>29</th><td>5.2</td><td>3.4</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>30</th><td>4.7</td><td>3.2</td><td>1.6</td><td>0.2</td><td>setosa</td></tr><tr><th>&vellip;</th><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td><td>&vellip;</td></tr></tbody></table>



As a data scientist, or an aspiring one, you must know what this data means, you must have a clear idea what an Iris flower is, what the columns means, if you do not have an idea, then try reading about it [here on Wikipedia](https://en.wikipedia.org/wiki/Iris_flower_data_set). There were some guys patient enough to collect different species of [iris](https://en.wikipedia.org/wiki/Iris_(plant)) flower, measure it's petal length, sepal length, petal width and sepal width, catalog it, so that who knows what they thought, now its benefiting machine learning learners enormously.

Now I am not sure if I did install Julia's `DataFrames` package, but `iris` is a DataFrame. DataFrame is a library created in Julia for easy visualization and manipulation of tabular data. If the following command gives you error, be sure to install [DataFrames.jl](https://dataframes.juliadata.org).

Now let's take a look at column names of `iris`.

```julia
names(iris)
```

So all columns shown below, except for _Species_ is a measure of length and width of a flower part. The last column is the name of the species.


    5-element Array{String,1}:
     "SepalLength"
     "SepalWidth"
     "PetalLength"
     "PetalWidth"
     "Species"

If you are not sure what petal and sepal is, I hope this image would help:

|![](https://upload.wikimedia.org/wikipedia/commons/7/7f/Mature_flower_diagram.svg)|
|:-:|
|[Parts of a flower](https://en.wikipedia.org/wiki/Flower#/media/File:Mature_flower_diagram.svg )|

Now let's take a look at size of the iris dataset:

```julia
size(iris)
```

It contains 150 rows and 5 columns as shown below:

    (150, 5)

Now let's apply a function called `describe()` on `iris`, it gives some statistical values on irises columns


```julia
describe(iris)
```

as shown below:


<table class="data-frame"><thead><tr><th></th><th>variable</th><th>mean</th><th>min</th><th>median</th><th>max</th><th>nmissing</th><th>eltype</th></tr><tr><th></th><th>Symbol</th><th>Union…</th><th>Any</th><th>Union…</th><th>Any</th><th>Int64</th><th>DataType</th></tr></thead><tbody><p>5 rows × 7 columns</p><tr><th>1</th><td>SepalLength</td><td>5.84333</td><td>4.3</td><td>5.8</td><td>7.9</td><td>0</td><td>Float64</td></tr><tr><th>2</th><td>SepalWidth</td><td>3.05733</td><td>2.0</td><td>3.0</td><td>4.4</td><td>0</td><td>Float64</td></tr><tr><th>3</th><td>PetalLength</td><td>3.758</td><td>1.0</td><td>4.35</td><td>6.9</td><td>0</td><td>Float64</td></tr><tr><th>4</th><td>PetalWidth</td><td>1.19933</td><td>0.1</td><td>1.3</td><td>2.5</td><td>0</td><td>Float64</td></tr><tr><th>5</th><td>Species</td><td></td><td>setosa</td><td></td><td>virginica</td><td>0</td><td>CategoricalValue{String,UInt8}</td></tr></tbody></table>


I hope one knows the meaning of statistical terms $$mean$$, $$median$$, $$min$$ and $$max$$. Ideally this blog series should explain what they are, may be in the future, for now please see other sources like Wikipedia for help.

If we want more of description of the `iris` data set, we need to pass a symbol `:all`. Once again this blog series should say what a symbol is in Julia. But for now passing `:all` will yield more statistical columns



```julia
describe(iris, :all)
```

as shown below:


<table class="data-frame"><thead><tr><th></th><th>variable</th><th>mean</th><th>std</th><th>min</th><th>q25</th><th>median</th><th>q75</th><th>max</th><th>nunique</th></tr><tr><th></th><th>Symbol</th><th>Union…</th><th>Union…</th><th>Any</th><th>Union…</th><th>Union…</th><th>Union…</th><th>Any</th><th>Union…</th></tr></thead><tbody><p>5 rows × 13 columns (omitted printing of 4 columns)</p><tr><th>1</th><td>SepalLength</td><td>5.84333</td><td>0.828066</td><td>4.3</td><td>5.1</td><td>5.8</td><td>6.4</td><td>7.9</td><td></td></tr><tr><th>2</th><td>SepalWidth</td><td>3.05733</td><td>0.435866</td><td>2.0</td><td>2.8</td><td>3.0</td><td>3.3</td><td>4.4</td><td></td></tr><tr><th>3</th><td>PetalLength</td><td>3.758</td><td>1.7653</td><td>1.0</td><td>1.6</td><td>4.35</td><td>5.1</td><td>6.9</td><td></td></tr><tr><th>4</th><td>PetalWidth</td><td>1.19933</td><td>0.762238</td><td>0.1</td><td>0.3</td><td>1.3</td><td>1.8</td><td>2.5</td><td></td></tr><tr><th>5</th><td>Species</td><td></td><td></td><td>setosa</td><td></td><td></td><td></td><td>virginica</td><td>3</td></tr></tbody></table>

If you are wondering what the $$q25$$ and $$q75$$ are, refer to [interquartile range](https://en.wikipedia.org/wiki/Interquartile_range). May be this blog series will one day explain it.

We can take a look at the first 5 elements of the data frame by using function called `first()` as shown below:


```julia
first(iris, 5)
```


<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>5 rows × 5 columns</p><tr><th>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr><tr><th>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr></tbody></table>

Similarly the last few rows can be viewed using function named `last()`. Below we view last five rows:


```julia
last(iris, 5)
```


<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>5 rows × 5 columns</p><tr><th>1</th><td>6.7</td><td>3.0</td><td>5.2</td><td>2.3</td><td>virginica</td></tr><tr><th>2</th><td>6.3</td><td>2.5</td><td>5.0</td><td>1.9</td><td>virginica</td></tr><tr><th>3</th><td>6.5</td><td>3.0</td><td>5.2</td><td>2.0</td><td>virginica</td></tr><tr><th>4</th><td>6.2</td><td>3.4</td><td>5.4</td><td>2.3</td><td>virginica</td></tr><tr><th>5</th><td>5.9</td><td>3.0</td><td>5.1</td><td>1.8</td><td>virginica</td></tr></tbody></table>

Now here is a way how to extract just petal lengths from `iris`.


```julia
iris[!, "PetalLength"]
```




    150-element Array{Float64,1}:
     1.4
     1.4
     1.3
     1.5
     1.4
     1.7
     1.4
     1.5
     1.4
     1.5
     1.5
     1.6
     1.4
     ⋮
     4.8
     5.4
     5.6
     5.1
     5.1
     5.9
     5.7
     5.2
     5.0
     5.2
     5.4
     5.1


So let's go on plotting and see if there is a way to separate species, for it we will just use 2 parameters, the `PetalLength` and `SepalLength`. I install Plots and started using it with these commands:

```julia
Pkg.add("Plots")
using Plots
```

    [32m[1m  Resolving[22m[39m package versions...
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Project.toml`
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Manifest.toml`

Now all I do is gather `SepalLength` in a variable `sepal_length`, `PetalLength` in `petal_length` and `Species` in `species`. Why on earth did I gather `Species`?! Anyway. I user the `scatter()` function to plot it as shwn below:


```julia
sepal_length = iris[!, "SepalLength"]
petal_length = iris[!, "PetalLength"]
species = iris[!, "Species"]
scatter(petal_length, sepal_length, color = "red")
```


![svg](/images/plots/iris_dataset_analysis/output_11_0.svg)


While you were viewing the Data Frame of `iris`, you must have seen just only 1 species in your Jupyter notebook. It tends to show only first 30 rows or something. Actually there are three different species that are tabulated in this data set. So in order to get the species name we can use this:

```julia
iris[!, "Species"]
```

Which just pulls out the species column. But we do not need 50 setosa's followed by 50 some other one, we just want to view unique species names. To get unique values from array, we could use `Set()` which pulls out the unique values. That's what we do below:

```julia
species = Set(iris[!, "Species"])
```




    Set{CategoricalArrays.CategoricalValue{String,UInt8}} with 3 elements:
      CategoricalArrays.CategoricalValue{String,UInt8} "versicolor"
      CategoricalArrays.CategoricalValue{String,UInt8} "setosa"
      CategoricalArrays.CategoricalValue{String,UInt8} "virginica"


In the code below, I thought of printing just the species name without all those clunky `CategoricalArrays.CategoricalValue{String,UInt8}` stuff, but I failed.


```julia
species_names = []

for x in species
    push!(species_names, x)
end

species_names
```




    3-element Array{Any,1}:
     CategoricalArrays.CategoricalValue{String,UInt8} "versicolor"
     CategoricalArrays.CategoricalValue{String,UInt8} "setosa"
     CategoricalArrays.CategoricalValue{String,UInt8} "virginica"



Once again my failed attempt to pretty print just a species name without the clunky `CategoricalArrays.CategoricalValue{String,UInt8}`

```julia
species_names[1]
```




    CategoricalArrays.CategoricalValue{String,UInt8} "versicolor"

It would be great if we can color our dots depending on the species. That might tell us something. So we need to split the dataset depending on the species. For that we can use the package `DataFramesMeta`. We install it as shown below:


```julia
using Pkg
Pkg.add("DataFramesMeta")
```

    [32m[1m  Resolving[22m[39m package versions...
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Project.toml`
    [32m[1mNo Changes[22m[39m to `~/.julia/environments/v1.5/Manifest.toml`

We use it as shown below:

```julia
using DataFramesMeta
```

One of the species name is versicolor. The `DataFramesMeta` package gives us the ability to filter a DataFrame to our need, so let's separate just the versicolor data.


```julia
iris_versicolor = @where(iris, :Species .== "versicolor")
first(iris_versicolor, 5)
```

In the code above, we use a function called `@where()`, well is it a function? I don't know, it starts with an `@`. As a  first argument we send in the data frame that needs filtering, so the code now becomes `@where(iris)`. As a second argument we send in the condition. Now look at the condition:

```julia
:Species .== "versicolor"
```

For now just think that `:Species` means `iris[!, "Species"]`, the colon `:` before the `Species` means that `:Species` is a Symbol. More on Symbols in later blogs may be.

Now look at this one `.==`, let's fire up Julia REPL and do this:

```julia
julia> ["setosa", "versicolor", "virginica", "setosa"] .== "versicolor"
4-element BitVector:
 0
 1
 0
 0
```

As you see above, when you do an array comparison with a `.==`, it compares each and every element of an array. So what matches becomes 1 and what doesn't becomes 0. This is used by `@where()` to filter out the rows whose `Species` is only versicolor, and we get a result, we store it in a variable named `iris_versicolor` and print its first five rows using `first(iris_versicolor, 5)`.

<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>5 rows × 5 columns</p><tr><th>1</th><td>7.0</td><td>3.2</td><td>4.7</td><td>1.4</td><td>versicolor</td></tr><tr><th>2</th><td>6.4</td><td>3.2</td><td>4.5</td><td>1.5</td><td>versicolor</td></tr><tr><th>3</th><td>6.9</td><td>3.1</td><td>4.9</td><td>1.5</td><td>versicolor</td></tr><tr><th>4</th><td>5.5</td><td>2.3</td><td>4.0</td><td>1.3</td><td>versicolor</td></tr><tr><th>5</th><td>6.5</td><td>2.8</td><td>4.6</td><td>1.5</td><td>versicolor</td></tr></tbody></table>

In a similar fashion, we filter out setosa:

```julia
iris_setosa = @where(iris, :Species .== "setosa")
first(iris_setosa, 5)
```


<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>5 rows × 5 columns</p><tr><th>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr><tr><th>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr><tr><th>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr><tr><th>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr></tbody></table>


and virginica:

```julia
iris_virginica = @where(iris, :Species .== "virginica")
first(iris_virginica, 5)
```

<table class="data-frame"><thead><tr><th></th><th>SepalLength</th><th>SepalWidth</th><th>PetalLength</th><th>PetalWidth</th><th>Species</th></tr><tr><th></th><th>Float64</th><th>Float64</th><th>Float64</th><th>Float64</th><th>Cat…</th></tr></thead><tbody><p>5 rows × 5 columns</p><tr><th>1</th><td>6.3</td><td>3.3</td><td>6.0</td><td>2.5</td><td>virginica</td></tr><tr><th>2</th><td>5.8</td><td>2.7</td><td>5.1</td><td>1.9</td><td>virginica</td></tr><tr><th>3</th><td>7.1</td><td>3.0</td><td>5.9</td><td>2.1</td><td>virginica</td></tr><tr><th>4</th><td>6.3</td><td>2.9</td><td>5.6</td><td>1.8</td><td>virginica</td></tr><tr><th>5</th><td>6.5</td><td>3.0</td><td>5.8</td><td>2.2</td><td>virginica</td></tr></tbody></table>

Now let's plot versicolor `PetalLength` vs `SepalLength` in red dots:

```julia
petal_lengths = iris_versicolor[!, "PetalLength"]
sepal_lengths = iris_versicolor[!, "SepalLength"]

petal_length_sepal_length_plot = scatter(
    petal_lengths,
    sepal_lengths,
    color= "red",
    label = "versicolor"
)
```

![svg](/images/plots/iris_dataset_analysis/output_20_0.svg)


Now we do for setosa in blue dots:


```julia
petal_lengths = iris_setosa[!, "PetalLength"]
sepal_lengths = iris_setosa[!, "SepalLength"]

scatter!(
    petal_length_sepal_length_plot,
    petal_lengths, sepal_lengths,
    color = "blue",
    label = "setosa"
)
```



![svg](/images/plots/iris_dataset_analysis/output_21_0.svg)


and virginica in green:

```julia
petal_lengths = iris_virginica[!, "PetalLength"]
sepal_lengths = iris_virginica[!, "SepalLength"]

petal_length_sepal_length_plot = scatter!(
    petal_length_sepal_length_plot,
    petal_lengths,
    sepal_lengths,
    color= "green",
    label = "virginica"
)
```


![svg](/images/plots/iris_dataset_analysis/output_22_0.svg)


Now let's give the plot a title and label its axis and put legend in top left so that it does not obstruct our plot:


```julia
scatter!(
    petal_length_sepal_length_plot,
    title = "Iris Data Set",
    xlabel = "Petal Length (cm)",
    ylabel = "Sepal Length (cm)",
    legend = :topleft
)
```


![svg](/images/plots/iris_dataset_analysis/output_23_0.svg)

One could get Jupyter notebook for this blog [here](https://gitlab.com/data-science-with-julia/code/-/blob/master/Iris%20Data%20Set%20Analysis.ipynb).

