---
title: "Plots: Installing Julia Plots"
layout: post
---
It is said that two third of our brains is devoted to process vision. We humans are very visual animals, we can detect patters from seeing things rather than looking at a table of numbers. In fact many Data Scientists make their living by creating just visualizations and info news so that people concerned can easily digest it and understand it. I don't think I will be going to very detail about visualizations, the path for these blogs are not decided yet, but let's here look at plotting in Julia.

There is a package called [Plots](https://docs.juliaplots.org/latest/) in Julia which you can install it as follows:

First launch the Julia REPL by typing in `julia` in your terminal.

```
$ julia
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.5.3 (2020-11-09)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/

julia>                    |
```

Go to packages by typing in `]` at the `julia> ` prompt, then in `pkg>` type this:


```julia
(@v1.5) pkg> add Plots

```

so should see lot of packages getting installed and finally `Plots` too will get installed. Now the notebook for this blog is here <https://gitlab.com/data-science-with-julia/code/-/blob/master/plots.ipynb>. It's not that you must install Plots from Julia REPL, you can also install it pragmatically as shown below, right from your Jupyter lab:

```julia
# Uncomment lines below if you want to install Plots
using Pkg
Pkg.add("Plots")
```

I have commented it out in the one you can get it from Gitlab because I have already installed it using the REPL. Okay lets tell Julia that we are using `Plots` with the following statement:

```julia
using Plots
```

Output:

    ┌ Info: Precompiling Plots [91a5bcdd-55d7-5caf-9e0b-520d859cae80]
    └ @ Base loading.jl:1278

This could take a while as it depends on the processing speed of your computer. For my 7-year laptop I think it took nearly 20 minutes, but once the compiling of `Plots` is doe, from the next time on its fast.

Now let's plot a simple pie chart which reflects my favorite food platter:

```julia
pie(
    ["Mutton Briyani", "Tandoori Chicken", "Prawn Roast", "Greens"],
    [60, 20, 10, 10],
    title = "My Favorite Plate"
)
```

Output:

![svg](/images/plots/pie_chart.svg)

We will see more of `Plots` in my upcoming blogs.
