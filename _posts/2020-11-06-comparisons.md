---
title: Comparisons
layout: post
---

{% youtube TCO6T8T8aE8 %}

Life is full of comparisons, we must earn more next year than now. We are considered lesser than our boss because he owns an expensive car, or has more money. Programming follows real life, comparisons are important part of programming too. Let's see how its done in Julia.

The notebook for this blog is available here <https://gitlab.com/data-science-with-julia/code/-/blob/master/comparisons.ipynb>. So fire your Jupyter lab and let's get started.

First let's see if 1 is greater than 2, for checking that we use greater than symbol `>`

```julia
1 > 2
```

Output:

    false

Looks like 1 is less than 2 in Julia world. Now let's check if 2 is greater than 1, and it looks so as from below example:

```julia
2 > 1
```

Output:

    true


Now let's see if 1 is greater than 1:

```julia
1 > 1
```

Output:

    false


Looks not, and it looks as though 1 is equal to 1 as we can see below. The `>=` checks if the left hand side number is either greater or equal to the right hand side number.

```julia
1 >= 1
```

Output:

    true

Now let's use the less than sign and try to do something. Let's check if 5 is less than 3 using the less than `<` operator

```julia
5 < 3
```

Output:

    false


Now lets check if 3 is less than 5

```julia
3 < 5
```

Output:

    true

Now let's see if 3 is less than or equal to 5 using the `<=` operator. Since 3 is less than 5 it returns true.

```julia
3 <= 5
```

Output:

    true


Now rather than putting `=` sign after `<`, we will put it before `<` and check.

```julia
3 =< 5
```

Output:

    syntax: "<" is not a unary operator



    Stacktrace:

     [1] top-level scope at In[8]:1

     [2] include_string(::Function, ::Module, ::String, ::String) at ./loading.jl:1091

     [3] execute_code(::String, ::String) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:27

     [4] execute_request(::ZMQ.Socket, ::IJulia.Msg) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/execute_request.jl:86

     [5] #invokelatest#1 at ./essentials.jl:710 [inlined]

     [6] invokelatest at ./essentials.jl:709 [inlined]

     [7] eventloop(::ZMQ.Socket) at /home/karthikeyan/.julia/packages/IJulia/a1SNk/src/eventloop.jl:8

     [8] (::IJulia.var"#15#18")() at ./task.jl:356


It fails.

Now let's check if 3 is less than or equal to 3.

```julia
3 <= 3
```

Output:

    true


Now let's check of 3 equals 3, for that we use the double equal to `==` operator as shown.

```julia
3 == 3
```

Output:

    true


Now look at the statement below,we are checking if `3 == 3.0`, that is `3` on the left side is an integer and `3.0` on the right side is a floating point number or a Real number. These two pieces of data are stored differently in Julia in different locations, yet Julia is smart enough to compare them and say they are equal. If you want to know more about floating point and how they are stored in computers, look here <https://en.wikipedia.org/wiki/Floating-point_arithmetic>

```julia
3 == 3.0
```

Output:

    true

If we want to check if two values are stored in same location in computers memory, we can use the triple equal sign `===` as shown. `3` is stored in different location  than `3.0` hence Julia says they both are not equal.

```julia
3 === 3.0
```

Output:

    false

Try this one

```julia
a = 5
b = 5
a === b

```

What do you infer from it?


Now there is a special value called `NaN` in Julia, it means not a number. Now it turns out that two `NaN`'s  are not equal

```julia
NaN == NaN
```

Output:

    false

But it turns out that in Julia all the things that are `NaN` point to a same address located in the RAM, so that passes the `===` test as shown:

```julia
NaN === NaN
```

Output:

    true


The last operator we are going to see in the blog is not equal to, denoted by `!=`, so let's check if 4 is not equal to 5

```julia
4 != 5
```

Output:

    true


Turns out true. Now let's check if 4 is not equal to 4:


```julia
4 != 4
```

Output:

    false
