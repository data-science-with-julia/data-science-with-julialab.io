---
title: Books I am reading to write this blog
layout: post
---


Writing a tech blog is hard. You have to refer a lot, and that's what I am doing. For this blog Data Science With Julia, I am referring few books and I wish to list them here


## Think Julia

![](https://learning.oreilly.com/library/cover/9781492045021/250w/)

**Website:** <https://benlauwens.github.io/ThinkJulia.jl/latest/book.html>

The first book is **Think Julia**. This a free book, available online, which can get you into programming domain. I use this book to learn about Julia and put the essence of what I learned here.

## Mathematics for Machine Learning

![](https://mml-book.github.io/static/images/mml-book-cover.jpg)

**Website:** <https://mml-book.github.io/>

Mathematics is Machine Learning and A.I, nothing else. In fact Galileo believed that all universe is mathematics, and it's true. This book **Mathematics for Machine Learning** is very popular and has reading groups etc.

Though I am a Machine Learning engineer, I wish to go to the depths of Machine Learning and hence I am reading this book. Ideally reading Think Julia and this book should suffice, but there are other books that I am reading, extracting concepts and will be putting in this blog.

## Data Science From Scratch

![](https://learning.oreilly.com/library/cover/9781492041122/250w/)

**Website:** <https://www.oreilly.com/library/view/data-science-from/9781492041122/>

**Data Science from Scratch** is a great book. In it you code your own Data Science algorithms, but it's in Python. I will read this book and write its concepts as blog and will put the code in Julia so that we could use it.

## Machine Learning in Action

![](https://images.manning.com/720/960/resize/book/c/4d7bb26-5e33-4b6a-b71b-2fa35958c0d9/pharrington.png)

**Website:** <https://www.manning.com/books/machine-learning-in-action>

I love this book **Machine learning in action**, I am not sure of the quality of code in this book. According to me, code should be clear. If I can read this book, explain concepts with same clarity and put code in a clear way using Julia, it will be good.

## Grokking Deep Learning

![](https://1.bp.blogspot.com/-f1biOfZmQmY/XwzS1KMfthI/AAAAAAAAXQI/2410mv-pISUz4sUmT6QaPOhNLOv1QxDWACLcBGAsYHQ/s1600/images%2B%252882%2529.jpeg)

**Website:** <https://www.manning.com/books/grokking-deep-learning>

It's dream of every ML engineer to build his or her own neural network. This book **Grokking Deep Learning** teaches you just that. But it's in Python. If I can read this book and put it here in Julia in clear and explainable terms, it would be great.

## Hands on Machine Learning

![](https://learning.oreilly.com/library/cover/9781492032632/250w/)

**Website:** <https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/>

If you want to start applying for ML jobs, this is the book to read. But this book uses age old Python. Python is still popular choice for ML engineers today, but I think it will change.

{% youtube 4I1ejhQqD4c %}

If I can read this book, put it in Julia and explain on how you can become a ML pro using Julia it would be great.

## Artificial Intelligence a Modern Approach

![](https://www.informit.com/ShowCover.aspx?isbn=0134610997)

**Website:** <http://aima.cs.berkeley.edu/global-index.html>

What to say? This book is a classic AI book and how could I leave it out. You must buy it if you can. For years this was the book that thought de-facto AI to generations of students. You need to know the history of AI too to understand about it, so this book is significant.

## Time consuming penance

![](https://img1.cgtrader.com/items/2368480/97623ce947/buddha-3d-model-low-poly-obj-fbx.jpg)

This blog is a time consuming penance, it's my attempt to learn machine learning thoroughly and share it with the world so that many could become like me. It could take years for me to do it, and I would be happy if you could stick around with me.
