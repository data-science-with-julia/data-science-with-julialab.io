---
title: Building Libraries
layout: post
---
In this blog, we are going to see how to build libraries in Julia. There might be many ways to do it, but this blogs shows how I would do it.

First look at this file <https://gitlab.com/data-science-with-julia/code/-/blob/master/gcd_and_lcm.ipynb>, download and run this jupyter notebook and look at the code. I will explain the code (though not in detail).

In the code below, we are writing a code to find GCD of a number, I am not going to explain how the algorithm works now, but take for granted this works.

```julia
a = 75
b = 120

min = a < b ? a : b
max = a > b ? a : b
reminder = max % min

while reminder != 0
    max = min
    min = reminder
    reminder = max % min
end

println("GCD = ", min)
```

Output

    GCD = 15

Change any value of `a` and `b` in the above program and you should be getting the GCD. Once you have got algorithm to get the GCD, getting LCM is simple as it can be computed with the following formula for given 2 numbers $$x$$ and $$y$$

$$LCM = {xy \over GCD}$$

If you have noticed, in the code we are using variables `a` and `b` instead of $$x$$ and $$y$$. So below is a program showing to computing both GCD and LCM for a pair of numbers

```julia
a = 75
b = 120

min = a < b ? a : b
max = a > b ? a : b
reminder = max % min

while reminder != 0
    max = min
    min = reminder
    reminder = max % min
end

gcd = min
lcm = (a * b) / gcd

println("GCD = ", gcd)
println("LCM = ", lcm)
```

Output

    GCD = 15
    LCM = 600.0

If you can see in this notebook <https://gitlab.com/data-science-with-julia/code/-/blob/master/gcd_and_lcm_with_functions.ipynb>, I have taken the GCD code and written it as a function:

```julia
function gcd(a, b)
    min = a < b ? a : b
    max = a > b ? a : b
    reminder = max % min

    while reminder != 0
        max = min
        min = reminder
        reminder = max % min
    end

    min
end
```

Now let's compute GCD of two numbers using this code.

```julia
gcd(3, 84)
```

Output

    3

Having known the formula for LCM, we write an elegant function to compute it:

```julia
function lcm(a, b)
    a * b / gcd(a, b)
end
```

Let's test out the `lcm` function


```julia
lcm(7, 25)
```

Output

    175.0

Seems to work.

Now let's create a Julia file and put both the functions in it, you can get the file here  <https://gitlab.com/data-science-with-julia/code/-/blob/master/gcd_lcm.jl>


```julia
function gcd(a, b)
    min = a < b ? a : b
    max = a > b ? a : b
    reminder = max % min

    while reminder != 0
        max = min
        min = reminder
        reminder = max % min
    end

    min
end

function lcm(a, b)
    a * b / gcd(a, b)
end
```

Now we have built our library, all we need to do is to include this file in Julia programs for us to have `gcd()` and `lcm()` functions readily available.


Let's create anew note book, you can get it here  <https://gitlab.com/data-science-with-julia/code/-/blob/master/gcd_lcm_from_file.ipynb>

Include the file `gcd_lcm.jl`

```julia
include("gcd_lcm.jl")
```

Output

    lcm (generic function with 1 method)


And let's find GCD of two numbers as shown:


```julia
gcd(17, 31)
```

Output


    1


And we find LCM too.


```julia
lcm(17, 31)
```

Output


    527.0

So in this blog we have created an algorithm to find GCD and LCM, made them into functions, built a library by putting the functions into files, and use the library in our code.

If you want to distribute your libraries, you must build a thing called packages, that might be in later blogs.


