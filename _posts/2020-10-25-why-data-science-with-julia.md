---
layout: post
title:  "Why Data Science With Julia?"
---

![](https://upload.wikimedia.org/wikipedia/commons/1/1f/Julia_Programming_Language_Logo.svg)

{% youtube NGMsM9_3sF0 %}

Hello, and welcome to my blog on Data Science, in these blog series, I will be learning the skill of Data Science, Machine Learning and Artificial Intelligence using Julia.

In this post I would like to explain why I am keeping this blog.

# A personal preference

In future, if I had to do Data Science and if the person or company that gives me the project, gives me the power to choose the language to be implemented, I would choose Julia. So when I do so, I might get some doubts, need some reference, and hence I would return to this blog for that.

# Julia is better than Python

Personally I have the opinion that Julia is better than Python for Scientific Computing, so despite the popularity of Python, I am choosing Julia. Julia is fast and hence all algorithms, right to the core can be coded in Julia.

Though Python is used for Data Science, it's mostly a wrapper around libraries written in C and other faster programming languages. It's okay o use Python if you want to finish a project and get a quick buck, but this blog is a place to learn. So I want to choose a superior one.

I have implemented same algorithms in Julia and Python, and found that Julia's implementation is really sleek, so I fell in love with it.

# From scratch with Mathematics

This blog will include math, a proper way learn Data Science and Artificial Intelligence is to learn math, and not avoiding it. This is a place where I will be explaining to myself about algorithms from scratch with as much details as possible. So it should be a great place if you want to know nuts and bolt's about Data Science.

# Guarantee for future

I am a Ruby on Rails web developer. A software architect in a huuuuuge company. But essentially web apps are nothing but insert and select operations on a Data Base with few dabs of business logic here and there. We make it look complex by inventing geeky terms like World Wide Web, Ajax, Cloud etc so that we can charge more for our services.

Building web apps is becoming cheaper and cheaper, and I want to do something more than push and pull values from database. I want to jump into scientific computing. I want to make this a place where if some one reads it, they should feel, yes this guys is passionate and knows this stuff. These blogs should guarantee my future job.

# Failed and stagnated attempts

I tried to write a Artificial Intelligence, Machine Learning and Data Science book with Julia, it failed. Perhaps I tried to do too much at a given time. There is a project called [GNU Ghost](https://gnughost.gitlab.io) which is stagnant because of my ignorance. I had a company called Trumatics which was supposed be a Data Science company, but I lost it because humans are weird and I had to return back to building websites.

All of the past is okay, life is full of failures; but is life life if I give up? I don't think so. What if this blog when read b a bright [Raychaudri](https://en.wikipedia.org/wiki/Amal_Kumar_Raychaudhuri) in the making

{% youtube i9_hm2qe34s %}

and whats if he invents something that would make me live another 2000 years. That's a sure win! In that case I can achieve all I missed out!! It's dreams like that is motivating this blog.

# Disclaimer

Though this blogs starts on a very high note, most of what I started stagnated and failed. So in case I am not updating it, try sending me emails, in case I read it I might write something. So let's see what the future holds.
