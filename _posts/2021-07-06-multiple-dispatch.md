---
title: functions 9 - Multiple Dispatch
layout: post
---

{% youtube fNz2IlpG41A %}

Take a look at the code below, type it and execute it in your Jupyter notebook or in a text file. Let's see how it works. If you see, we have defined a functions named `multi_disp` many times:

```julia
function multi_disp()
    println("In multi_disp()")
end

function multi_disp(some_argument)
    println("In multi_disp(some_argument)")
end

function multi_disp(number::Int)
    println("In multi_disp(number::Int)")
end

function multi_disp(arg1, arg2)
    println("In multi_disp(arg1, arg2)")
end


multi_disp()
multi_disp("abc")
multi_disp(70)
multi_disp(1, 2)
```

Output

    In multi_disp()
    In multi_disp(some_argument)
    In multi_disp(number::Int)
    In multi_disp(arg1, arg2)

So if you see this piece of code

```julia
multi_disp()
multi_disp("abc")
multi_disp(70)
multi_disp(1, 2)
```

one can see `multi_disp()` calling this function definition:

```julia
function multi_disp()
    println("In multi_disp()")
end
```

Notice that the function definition has no arguments, and `multi_disp()` has no arguments too!

Now take for instance we are calling `multi_disp("abc")`, some how this executes this function:

```julia
function multi_disp(some_argument)
    println("In multi_disp(some_argument)")
end
```

and not any other function link this one:

```julia
function multi_disp(number::Int)
    println("In multi_disp(number::Int)")
end
```

Julia knows that `function multi_disp(number::Int)` takes an integer for argument and hence should be avoided when we call `multi_disp("abc")` where `"abc"` is a string. But when `multi_disp(70)` is called, Julia rightly executes `multi_disp(number::Int)` because we are passing in number. This is called multiple dispatch, and it is one of the most powerful feature that Julia offers to programmers. We will see it when we create complex programs where a same function needs to do multiple things depending on number of arguments and types of arguments varies.

Now for `multi_disp(1, 2)`, Julia rightly calls:

```julia
function multi_disp(arg1, arg2)
    println("In multi_disp(arg1, arg2)")
end
```

Because the above function has two arguments. So Julia takes into account the number of arguments and the arguments types when trying to decide which function to call when function names are same.

Get the notebook for this blog here <https://gitlab.com/data-science-with-julia/code/-/blob/master/multiple%20dispatch.ipynb>.
