---
title: Files
layout: post
---

{% youtube hNOFSCT4U00 %}

**Contents**
{:.no_toc}

* TOC
{:toc}

## Writing Files

Let's say you have a variable say `a = 42` in Julia. How long do you think it will persist? Its stored in computer's [RAM](https://en.wikipedia.org/wiki/Random-access_memory), that is the volatile memory and would vanishes once the program get's terminated. RAM will be cleaned then and there, its a very fast memory that computer uses so that trips to [Hard Disk (HD)](https://en.wikipedia.org/wiki/Hard_disk_drive) or [Solid State Drive (SSD)](https://en.wikipedia.org/wiki/Solid-state_drive) will be reduced.

In case the reader is not aware, though HD and SSD can store data permanently for long time, they are slow compared to RAM. The reason why they are used because they require less amount of energy to store massive amounts of data compared to RAM, and they can store it for long.

I envision a future where RAM's will become so efficient, that we will have only one single fast  nonvolatile storage for computers. This will reduce cost as designing hardware and writing software will change for ever.


Let's come back to our topic. To persist our data for long time, we need to store it in files, and in this blog we are going to see how to read and write files in Julia.

Launch a Jupyter notebook, types these lines in a cell and execute it.


```julia
file = open("file_to_write.txt", "w")
write(file, "Hello files from Julia!")
close(file)
```

You should be seeing a file named `file_to_write.txt` in the folder where the Jupyter notebook is, when you open it, you should be able to see this content:

```
Hello files from Julia!
```

So what have we done here? We have opened a file named `file_to_write.txt` using the `open()` function.

```julia
open("file_to_write.txt")
```

We have opened it in write mode, that is we can put put content into the file in write mode. To say that we are opening a file in write mode we use the flag `"w"` as shown below:

```julia
open("file_to_write.txt", "w")
```

We have assigned the opened file file to a variable called `file` as shown below:

```julia
file = open("file_to_write.txt", "w")
```

This is because we can do something with this variable `file` (technically programmers call this file handle, if you call so the guy who interviews you could be impressed).


Next we write content in the file as shown below:

```julia
file = open("file_to_write.txt", "w")
write(file, "Hello files from Julia!")
```

In this we use the `write()` function. The first argument that goes into is the file handle, here it's the variable `file`, the second argument that goes into it is the content `"Hello files from Julia!"`.

Any file that is opened should be closed, and so we close it using `close(file)` as shown below.

```julia
file = open("file_to_write.txt", "w")
write(file, "Hello files from Julia!")
close(file)
```

So if we see, we can see a file named `file_to_write.txt` in our folder.


|![](/images/files/file_to_write.png)|
|:-:|
|file_to_write.txt seen in left panel of Jupyter lab|


### The open block  

You can avoid closing a file explicitly if you can write a file in an open block as shown:

```julia
open("second_file_to_write.txt", "w") do file
    write(file, "Hello files from Julia!")
end
```

Output:


    23

I think the 23 is the length of number of characters written into the file using the `write()` function. So in the above program one can think this code:

```julia
open("second_file_to_write.txt", "w") do file
end
```
does the equivalent of this one:

```julia
file = open("file_to_write.txt", "w")
close(file)
```

All we need to do now is put a `write()` function within the block so that the `file` get's written as shown:

```julia
open("second_file_to_write.txt", "w") do file
    write(file, "Hello files from Julia!")
end
```

### Writing multiple times

Now let's try to write multiple times in a file. type the code below and execute:

```julia
open("appending_in_write_mode.txt", "w") do file
    write(file, "Hello files from Julia!\n")
    write(file, "This is the second line.")
end
```

Output:

    24

I think the output 24 is the length of the last text written. Any way in the code above we execute two `write()` functions:

```julia
write(file, "Hello files from Julia!\n")
write(file, "This is the second line.")
```

In a file open block:

```julia
open("appending_in_write_mode.txt", "w") do file
    write(file, "Hello files from Julia!\n")
    write(file, "This is the second line.")
end
```

to write into files multiple times.

If yo open the file `appending_in_write_mode.txt` you should be able to see both lines in it.

## Appending files

Let's say that you want to append, that is add more content to already written file, let's see how to do it. First let's open a file called `appending_in_write_mode_gone_wrong.txt`in write mode and put some text into it as shown:

```julia
open("appending_in_write_mode_gone_wrong.txt", "w") do file
    write(file, "Hello files from Julia!\n")
end
```

Now let's open the file again and put more text in it as shown:

```julia
open("appending_in_write_mode_gone_wrong.txt", "w") do file
    write(file, "This is the second line.")
end
```

Output:


    24

Note, we are opening the second time in write `"w"` mode. When you check the file content, you don't see lines like these:

```
Hello files from Julia!
This is the second line.
```
But you just see `This is the second line.`. What does this tell us? Well if you want to append some content into a file, open it in append mode. That's what we are going to do next.

### Appending cotent in right way

```julia
open("appending_file.txt", "w") do file
    write(file, "Hello files from Julia!\n")
end

open("appending_file.txt", "a") do file
    write(file, "This is the second line.")
end
```

Output:


    24

In the above code, look at the second time we want to write content to a file, that is when we want to append it, we open it in append mode using the `"a"` flag as you can see in the line `open("appending_file.txt", "a") do file`. This ensures the writing starts at the end of the file, hence the content that was originally in it hasn't got overwritten.

When you open `appending_file.txt`, you should be able to see the following text:

```
Hello files from Julia!
This is the second line.
```

## Reading files

We have written something into `appending_file.txt`, now let's read it. To read a file, we will open it in trading mode with a `"r"` flag as shown below:

```julia
file_to_read = open("appending_file.txt", "r")
first_line = readline(file_to_read)
first_line
```

Output:


    "Hello files from Julia!"

The file handle is stored in `file_to_read` and we read the first line using the `readline()` function like this: `first_line = readline(file_to_read)`. Finally we inspect the first line and its `"Hello files from Julia!"`.

Now let's read the second line. If you see the above code we haven't closed `file_to_read`, and it has a memory that we have read the first ine, now to read the second line we need to call `readline()` as shown below:


```julia
second_line = readline(file_to_read)
second_line
```


Output:

    "This is the second line."

In the code above, the second line is stored in a variable called `second_line` and we inspect it.

Now let's read the third line:


```julia
third_line = readline(file_to_read)
```


Output:

    ""


There is no third line, and hence we get an empty string, same is the case for trying to read fourth line as shown below:


```julia
fourth_line = readline(file_to_read)
```

Output:


    ""


Finally let's close the file:


```julia
close(file_to_read)
```

### Reading all lines at once

Rather than reading line by line, we can read all lines in one go. First let's open a file:


```julia
file_to_read = open("appending_file.txt", "r")
```


Output:

    IOStream(<file appending_file.txt>)


Now we use `readlines()` on the file handle `file_to_read`, and variable `all_lines` points to  a vector that stores all the lines as shown:

```julia
all_lines = readlines(file_to_read)
all_lines
```


Output:

    2-element Vector{String}:
     "Hello files from Julia!"
     "This is the second line."


Finally we close the file:

```julia
close(file_to_read)
```

**Jupyter notebook for this blog**

<https://gitlab.com/data-science-with-julia/code/-/blob/master/files.ipynb>
