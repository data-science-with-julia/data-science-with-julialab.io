---
title: Iris dataset analysis - building a model
layout: post
---

In previous blog we generated a graph of iris data as shown:

![svg](/images/plots/iris_dataset_analysis/output_23_0.svg)

just eyeballing it we can say that just with `PetalLength` we could say for near certainty what species it is with a small error.

We can form an algorithm as follows.

1. if petal length is less than 2.5 cm say its setosa.
2. If the petal length is greater than 2.5cm and less than 4.8 cm say its versicolor.
3. Else say it's virginica.

There might be slight wrap with versicolor and virginica, but that's okay.


