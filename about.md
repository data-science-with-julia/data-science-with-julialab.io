---
title: About
layout: post
---

Hello, I'm Karthikeyan A K, a programmer from Chennai, India. To know more about me visit <https://mindaslab.github.io>. If you are using Telegram join my data science and artificial intelligence group here <https://t.me/ds_ai>.


The best way to contact me is on email <mindaslab@protonmail.com> (works no matter where I am).

## Getting the code

To get the code for this journey visit here <https://gitlab.com/data-science-with-julia/code>.

## Video's

There is a video series I'm doing for Data Making which kind of hugs to this blog, you can watch those video's here <https://www.youtube.com/playlist?list=PLe1T0uBrDrfOLQlomF_4AxHa4LX0wsCXa>

The ntebooks that are used in videos can be found here <https://gitlab.com/data-science-with-julia/video-notebooks>

